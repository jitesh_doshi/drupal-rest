; This file will build the entire project site directory
; Run it as ...
; drush make --no-core --contrib-destination=sites/rest drush.make foobar
; .. and then move foobar/sites/rest to your drupal-7/sites directory, after which foobar directory can be
;discarded.

core = 7.x
api = 2

projects[devel][version] = "1.3"
projects[devel][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "1.7"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[libraries][version] = "2.1"
projects[libraries][subdir] = "contrib"

projects[services][version] = "3.3"
projects[services][subdir] = "contrib"
