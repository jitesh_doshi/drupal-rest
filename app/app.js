function NodeListCtrl($scope) {
  $scope.nodes = [];

  $scope.activeNode = {'body': {'und': [{'value': 'No node selected.'}]}}

  $scope.selectNode = function(n) {
    console.log('Node selected', n);
    $.getJSON('/r/node/' + n.nid, {}, function(data) {
      $scope.activeNode = data;
      $scope.$apply();
    });
  }

  $scope.load = function() {
    $.getJSON('/r/node', {}, function(data) {
      $scope.nodes = data;
      $scope.$apply();
    });
  }
}

function NodeEditCtrl($scope) {
  $scope.type = "article";
  $scope.title = "node title";
  $scope.body = "node body";
  $scope.submit = function() {
    var node = {
      'type': $scope.type,
      'title': $scope.title
    };
    node.body = {'und': [{'value': $scope.body}]};
    $.ajax({
      type: "POST",
      url: '/r/node',
      data: node,
      headers: {'Accept': 'application/json'},
      success: function(data) {
        var msg = "Created node. Nid = " + data.nid;
        console.log(msg);
        $scope.log += msg + "\n";
        $scope.$apply();
      }
    });
  }
}

function LoginCtrl($scope) {
  $scope.login = function() {
    var data = {
      'username': $scope.username,
      'password': $scope.password
    };
    $.ajax({
      type: "POST",
      url: '/r/user/login',
      data: data,
      headers: {'Accept': 'application/json'},
      success: function(data) {
        console.log('data', data);
      }
    });
  }

  $scope.logout = function() {
    var data = {};
    $.ajax({
      type: "POST",
      url: '/r/user/logout',
      data: data,
      headers: {'Accept': 'application/json'},
      success: function(data) {
        console.log('data', data);
      }
    });
  }
}
