#!/bin/bash

if [ ! -f 'local.settings.php' ]; then
  echo 'You must copy local.settings.php.example to local.settings.php and edit it correctly before proceeding.'
  exit 1
fi

sudo chgrp -R www-data files

#download and enable modules
if [ ! -d modules/contrib ]; then
  drush make --no-core drush.make .drush-make
  mv .drush-make/sites/all/modules/contrib modules/contrib
  #mv .drush-make/sites/all/libraries/* libraries
  #mv .drush-make/sites/all/themes/zen themes
  rm -rf .drush-make
fi

drush site-install --site-name='Drupal REST'

#disable unnecessary modules
drush -y pm-disable update,overlay,dashboard,rdf,toolbar,shortcut,color

#developer modules & admin modules
drush -y en devel,devel_generate,admin_menu,module_filter

drush -y en rest_custom

#generate 50 nodes with up to 5 comments each
drush generate-content --types=article 50 5

echo '********** LOGIN LINK **************'
drush user-login

echo '********** REST TESTING **************'
echo 'LOGIN:  curl --cookie-jar ~/cookie.txt `drush -l http://this-server user-login`'
echo 'LIST:   curl --cookie ~/cookie.txt -H 'Accept: application/json' http://this-server/r/node'
echo 'CREATE: curl --cookie ~/cookie.txt -H 'Accept: application/json' http://this-server/r/node --data type=article --data title=t1 --data body[und][0][value]="my body"'
